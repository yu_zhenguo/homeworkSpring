package com.lagou.edu.demo.ioc.service;

public interface IDemoService {

    String get(String username);

    String securityTest(String username);
}
