package com.lagou.edu.demo.mvc.controller;


import com.lagou.edu.demo.ioc.service.IDemoService;
import com.lagou.edu.ioc.annouation.LagouAutowired;
import com.lagou.edu.mvc.annouation.LagouController;
import com.lagou.edu.mvc.annouation.LagouRequestMapping;
import com.lagou.edu.mvc.annouation.LagouSecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@LagouController
@LagouRequestMapping("/demo")
public class DemoController {


    @LagouAutowired
    private IDemoService demoService;

    @LagouSecurity(value = "xiaoMing")
    @LagouRequestMapping("/handle01")
    public String handle01(HttpServletRequest request, HttpServletResponse response, String username) {
        return demoService.get(username);
    }

    @LagouSecurity(value = "zhangSan,LiSi")
    @LagouRequestMapping("/handle02")
    public String handle02(HttpServletRequest request, HttpServletResponse response, String username) {
        return demoService.get(username);
    }
}
