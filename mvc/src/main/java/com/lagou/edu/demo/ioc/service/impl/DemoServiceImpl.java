package com.lagou.edu.demo.ioc.service.impl;


import com.lagou.edu.demo.ioc.service.IDemoService;
import com.lagou.edu.ioc.annouation.LagouService;

@LagouService("demoService")
public class DemoServiceImpl implements IDemoService {
    @Override
    public String get(String username) {
        System.out.println("service 实现类中的name参数：" + username) ;
        return username;
    }

    @Override
    public String securityTest(String username) {
        System.out.println("路径权限测试，name参数：" + username) ;
        return username;
    }
}
