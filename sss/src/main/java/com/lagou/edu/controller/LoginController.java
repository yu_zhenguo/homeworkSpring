package com.lagou.edu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/login")
public class LoginController {

    @RequestMapping("/loginIn")
    public String  loginIn(String username, String password, HttpSession session) throws Exception {
        if (username.equals("admin") && password.equals("admin")) {
            String user = username.concat("|").concat(password);
            session.setAttribute("USER_SESSION",user);
            return "redirect:/resume/queryAll";
        }
        return "redirect:login?isError=1";
    }

    @RequestMapping("/login")
    public ModelAndView  login(Integer isError) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        if (isError != null && isError == 1) {
            modelAndView.addObject("msg", "用户名或密码错误！");
        }
        return modelAndView;
    }

    @RequestMapping("/loginOut")
    public ModelAndView  loginOut(HttpSession session) throws Exception {
        session.invalidate();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        modelAndView.addObject("msg", "退出成功！");
        return modelAndView;
    }
}
