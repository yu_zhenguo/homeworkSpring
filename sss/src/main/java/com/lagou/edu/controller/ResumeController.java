package com.lagou.edu.controller;

import com.lagou.edu.pojo.Resume;
import com.lagou.edu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/resume")
public class ResumeController {

    /**
     * Spring容器和SpringMVC容器是有层次的（父子容器）
     * Spring容器：service对象+dao对象
     * SpringMVC容器：controller对象，，，，可以引用到Spring容器中的对象
     */


    @Autowired
    private ResumeService resumeService;

    @RequestMapping("/queryAll")
    public ModelAndView  queryAll(Integer state) throws Exception {
        List<Resume> list = resumeService.queryResumeList();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("list");
        modelAndView.addObject("list", list);
        modelAndView.addObject("state", state);
        return modelAndView;
    }

    @RequestMapping("/save")
    public String save(Resume resume) throws Exception {
        Long id = resume.getId();
        resumeService.saveResume(resume);
        return "redirect:queryAll?state=" + (id==null?1:2);
    }

    @RequestMapping("/deleteById")
    public String  deleteById(Long id) throws Exception {
        resumeService.deleteById(id);
        return "redirect:queryAll?state=0";
    }
}
