<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" isELIgnored="false" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Insert title here</title>
    <style>
        /* 弹窗 (background) */
        .modal {
            display: none; /* 默认隐藏 */
            position: fixed; /* 固定定位 */
            z-index: 1; /* 设置在顶层 */
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgb(0,0,0);
            background-color: rgba(0,0,0,0.4);
        }

        /* 弹窗内容 */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* 关闭按钮 */
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
</head>
<body>
<!-- 打开弹窗按钮 -->
<button style="margin-left: 450px;" id="myBtn">新增</button></td>
<a href="/login/loginOut">退出</a>
<table style="width: 500px;" border="1" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <th>姓名</th><th>电话</th><th>地址</th><th>操作</th>
    </tr>
    <c:forEach items="${list}" var="v" >
    <tr id="tr${v.id}">
        <td  id="td${v.id}Name" style="text-align: center">${v.name}</td>
        <td  id="td${v.id}Phone" style="text-align: center">${v.phone}</td>
        <td  id="td${v.id}Address" style="text-align: center">${v.address}</td>
        <td style="text-align: center"><%--${v.id},${v.name},${v.phone}, ${v.address}--%>
            <a href="javascript:void(0);" onclick="edit(${v.id})" >修改</a>
            <a href="/resume/deleteById?id=${v.id}">删除</a>
        </td>
    </tr>
    </c:forEach>
    </tbody>
</table>
<!-- 弹窗 -->
<div id="myModal" class="modal" style="width: 520px;">
    <span class="close">&times;</span>
    <!-- 弹窗内容 -->
    <div class="modal-content">
        <form method="post" action="/resume/save">
            <input id="id" type="hidden" name="id"/>
            姓名：<input id="name" type="text" name="name"/>
            </br>
            电话：<input id="phone" type="text" name="phone"/>
            </br>
            地址：<input id="address" type="text" name="address"/>
            </br>
            <input type="submit" value="提交"/>
        </form>
    </div>
</div>
</body>
<script language="JavaScript">
    if (${state == 0}) {
        alert("删除成功！")
        window.location.href = "queryAll";
    } else if(${state == 1}){
        alert("新增成功！")
        window.location.href = "queryAll";
    } else if(${state == 2}){
        alert("更新成功！")
        window.location.href = "queryAll";
    }

    // 获取弹窗
    var modal = document.getElementById('myModal');

    // 打开弹窗的按钮对象
    var btn = document.getElementById("myBtn");

    // 获取 <span> 元素，用于关闭弹窗
    var span = document.querySelector('.close');

    // 点击按钮打开弹窗
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // 点击 <span> (x), 关闭弹窗
    span.onclick = function() {
        modal.style.display = "none";
    }

    // 在用户点击其他地方时，关闭弹窗
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    
    function edit(id) {
        var tdName = document.getElementById('td'+id + 'Name').innerText;
        var tdPhone = document.getElementById('td'+id + 'Phone').innerText;
        var tdAddress = document.getElementById('td'+id + 'Address').innerText;

        document.getElementById("id").value = id;
        document.getElementById("name").value = tdName;
        document.getElementById("phone").value = tdPhone;
        document.getElementById("address").value = tdAddress;
        modal.style.display = "block";
    }
</script>
</html>